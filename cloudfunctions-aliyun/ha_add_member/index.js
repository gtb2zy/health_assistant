"use strict";
exports.main = async (event, context) => {
  //event为客户端上传的参数
  const db = uniCloud.database();
  const collection = db.collection("ha_group_person");
  // ha_group_person
  // {
  //     _id: "", // string，自生成
  //     group_id: "", // string 团体ID
  //     person_name: "", // string 成员名字
  //     person_identity: "", // string 成员身份证号
  //     person_sex: 0, // int, 性别1男2女3保密
  //     person_num: 0, // int 成员编号（学号、工号等）
  //     person_remark: "", // string 成员备注（如部门中的某分组不再建立团体，就写入备注）
  //     // 其他需要备注的信息：如按楼组成的团体，可写入门牌号码
  //     order: 1, // int，排序
  // }
  if (!event.group_id) {
    return {
      code: -2,
      msg: "当前团队id错误，请返回！"
    };
  } else if (!event.person_name) {
    return {
      code: -1,
      msg: "成员名字有误！"
    };
  } else if (!event.person_identity || !(/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/.test(event.person_identity))) {
    return {
      code: -1,
      msg: "成员身份证号有误，请重新输入！"
    };
  } else if (!event.person_sex) {
    return {
      code: -1,
      msg: "成员性别不能为空！"
    };
  } else if (!event.person_num) {
    return {
      code: -1,
      msg: "成员编号有误！"
    };
  } else {
    let data = await collection.add(event);

    return {
      code: 0,
      msg: "success"
    };
  }
};
