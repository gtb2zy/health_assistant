"use strict";
exports.main = async (event, context) => {
	const db = uniCloud.database();
	const collection = db.collection("ha_group_list");
	// return await collection.get()

	//event为客户端上传的参数
	// _id: "", // string，自生成
	// parent_id: "", // 所属ID
	// name: "", // 团体名称，如"四（3）班"、"软件2组"、"财务部"、"xx公司"等
	// label: "", // 标签，提供几个常用的选择，如学校、班级、公司、部门等
	// account_id: "", // string 管理员账号ID（用户账号）
	// is_reg: true, // int 是否开放成员注册 true开放
	// is_split: false, // int 是否分开 true分开：断开本团体的上级和下级的关系，公司、学校等使用
	// <!-- people_total: 0, // 成员总数，TODO，待删除 -->
	// order: 1, // int，排序
	if (JSON.stringify(event) === "{}") {
		return {
			code: -1,
			msg: "当前填写的信息为空！"
		};
	} else {
		if (!event.name) {
			return {
				code: -1,
				msg: "团体名不能为空！"
			};
		} else if (!event.label) {
			return {
				code: -1,
				msg: "标签不能为空！"
			};
		} else if (!event.account_id) {
			// -2=账号出现异常，重新登陆！
			return {
				code: -2,
				msg: "账号已过期，请重新登陆！"
			};
		} else {
			let data = await collection.add(event);
			return {
				code: 0,
				data,
				msg: "团体创建成功！"
			};
		}
	}
};
