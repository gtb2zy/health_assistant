import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection1 = db.collection('ha_report_list');
	let res1 = await collection1
		.doc(
			event.report_id
		)
		.get();

	if (res1.data && res1.affectedDocs === 1){
	} else {
		return {
			status: -2,
			msg: "抱歉，报备不存在！"
		}
	}

	if (!event.person_name) {
		return {
			status: -1,
			msg: '姓名有误！'
		}
	} else if (!event.person_identity ||
		!/^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/
		.test(event.person_identity)) {
		return {
			status: -1,
			msg: '身份证号有误，请重新输入！'
		}
	}

	const collection = db.collection('ha_group_person');
	let user = await collection
		.where({
			group_id: res1.data[0].group_id,
			// person_name: event.person_name.trim(),
			person_name: event.person_name,
			person_identity: event.person_identity
		})
		.get();

	if (user.data && user.affectedDocs >= 1){
	} else {
		return {
			status: -1,
			msg: "团体成员不存在！"
		}
	}

	let data = {
		gp_id: user.data[0]._id,
		gp_info: user.data[0].person_name,
		schema: res1.data[0].content
	};

	return {
		status: 0,
		data,
		msg: "获取成功"
	}
}

export {
	Perform as main
}
