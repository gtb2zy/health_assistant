import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import groupFunc from '../../utils/groupFunc.js';

// type === 0 : 断开团体关系
// type === 1 : 建立团体关系

async function Perform(event) {
	if (event.type === 0) {
		await groupFunc.updateGroupParent(event.group_id,"");
	} else if (event.type === 1){
		const {
			group_id,
			parent_id
		} = event;
		await groupFunc.updateGroupParent(group_id,parent_id);
	}

	return {
		status: 0,
		msg: "更新成功！"
	}
}

export {
	Perform as main
}
