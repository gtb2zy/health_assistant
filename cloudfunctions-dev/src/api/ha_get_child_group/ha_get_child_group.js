import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

import groupFunc from '../../utils/groupFunc.js';

async function Perform(event) {
	return await groupFunc.getFirstChildGroup(event.group_id);
}

export {
	Perform as main
}
