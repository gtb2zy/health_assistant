import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database()

async function signUp(event) {
	const {
		username,
		password,
	} = event

	let userInfo = {
		username
	}

	const userInDB = await db.collection('ha_user_account').where({
		username,
		password: encryptPassword(password),
	}).get()
	
	let tokenSecret = crypto.randomBytes(16).toString('hex'),
		token = jwt.encode(userInfo, tokenSecret)
	let userUpdateResult
	if (userInDB.data && userInDB.data.length === 0) {
		return {
			status: -1,
			msg: '用户名或密码不正确'
		}
	} else {
		userUpdateResult = await db.collection('ha_user_account').doc(userInDB.data[0]._id).update({
			tokenSecret,
			exp: Date.now() + tokenExp
		})
	}

	if (userUpdateResult.id || userUpdateResult.affectedDocs === 1) {
		return {
			status: 0,
			uid: userInDB.data[0]._id,
			token,
			msg: '登录成功'
		}
	}

	return {
		status: -1,
		msg: '登录失败'
	}
}

export {
	signUp as main
}
