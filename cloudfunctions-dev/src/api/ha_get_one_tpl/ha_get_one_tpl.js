import crypto from 'crypto'
import jwt from 'jwt-simple'
import {
	tokenExp
} from '../../utils/constants.js'

import encryptPassword from '../../utils/encryptPassword.js';

const db = uniCloud.database();

async function Perform(event) {
	const collection = db.collection('ha_template_list');
	let res = await collection
		.doc(
			event.tpl_id
		)
		.get();

	if (res.data && res.affectedDocs === 1) {
		if (res.data.length === 1) {
			return {
				status: 0,
				data: res.data[0],
				msg: "获取成功"
			}
		}
	}

	return {
		status: -1,
		msg: "模板不存在"
	}
}

export {
	Perform as main
}
