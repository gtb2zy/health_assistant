import {
	groupDepth
} from './constants.js'

const db = uniCloud.database();
const collection = db.collection('ha_group_list');

async function getChildGroupID(group_id,depth) {
	if ( depth >= groupDepth ) {
		return [];
	}

	let result = [];
	depth += 1;

	let res = await collection
		.where({
			parent_id: group_id
		})
		.get();

	for ( let i = 0; i < res.data.length; i++ ) {
		result.push(res.data[i]._id);

		if ( res.data[i].hasOwnProperty('is_split') == false || res.data[i].is_split != true ) {
			result = result.concat(await getChildGroupID(res.data[i]._id,depth));
		}
	}

	return result;
}

async function getFirstChildGroup(group_id) {
	let res = await collection
		.where({
			parent_id: group_id
		})
		.get();

	return res.data;
}

async function updateGroupParent(group_id,parent_id) {
	await collection
		.doc(group_id)
		.update({
			parent_id
		});
}

async function deleteOneGroup(group_id) {

	await db.collection('ha_group_person')
		.where({
			group_id
		})
		.remove();

	// await db.collection('ha_group_notice')
	// 	.where({
	// 		group_id
	// 	})
	// 	.remove();

	await db.collection('ha_report_list')
		.where({
			group_id
		})
		.remove();

	// await db.collection('ha_report_summary')
	// 	.where({
	// 		group_id
	// 	})
	// 	.remove();

	await collection
		.doc(group_id)
		.remove();
}

export default {
	getChildGroupID,
	getFirstChildGroup,
	updateGroupParent,
	deleteOneGroup
}